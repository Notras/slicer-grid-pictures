<?php
  $upload = 0;
  $jpg = 0;
  
  // Create TMP directory if necessary
  if(!is_dir('./tmp/')) {
    mkdir('./tmp/');
  }
  
  // Create LOGO if necessary
  if(!is_file('./logo.jpg')) {
    $logoImg = imagecreatetruecolor(34,22);
    $logoWhite = imagecolorallocate($logoImg, 255, 255, 255);
    imagefilledrectangle($logoImg,10,0,10,21,$logoWhite);
    imagefilledrectangle($logoImg,22,0,22,21,$logoWhite);
    imagefilledrectangle($logoImg,0,10,33,10,$logoWhite);
    imagejpeg($logoImg, './logo.jpg', 100);
    imagedestroy($logoImg);
  }
  
  if(!empty($_FILES) && isset($_POST['grid'])) {
    $grid = htmlspecialchars($_POST['grid']);
    $newdir = bin2hex(random_bytes(25));
    $tmpDir = './tmp/'.$newdir.'/';
    $tmpDirFull = __DIR__.'/tmp/'.$newdir.'/';
    $upload = 1;
    mkdir($tmpDir);
    
    $baseFileName = bin2hex(random_bytes(20));
    
    $srcFileName = $baseFileName.'_src'.'.tmp';
    $srcFile = $tmpDirFull.$srcFileName;
    move_uploaded_file($_FILES['picture']['tmp_name'], $srcFile);
    
    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    $finfo_mime = finfo_file($finfo, $srcFile);
    if($finfo_mime == 'image/jpeg') {
      $jpg = 1;
      $source_img = imagecreatefromjpeg($srcFile);
      $sourceX = imagesx($source_img);
      $pixels = floor($sourceX / 3);
      
      switch ($grid) {
        case '2x3':
          //From left to right, top to bottom
          // 1 - 2 - 3
          // 4 - 5 - 6
          $con = array();
          $con[1] = array('x' => 0, 'y' => 0, 'width' => $pixels, 'height' => $pixels);
          $con[2] = array('x' => $pixels, 'y' => 0, 'width' => $pixels, 'height' => $pixels);
          $con[3] = array('x' => $pixels * 2, 'y' => 0, 'width' => $pixels, 'height' => $pixels);
          $con[4] = array('x' => 0, 'y' => $pixels, 'width' => $pixels, 'height' => $pixels);
          $con[5] = array('x' => $pixels, 'y' => $pixels, 'width' => $pixels, 'height' => $pixels);
          $con[6] = array('x' => $pixels * 2, 'y' => $pixels, 'width' => $pixels, 'height' => $pixels);
          foreach ($con as $key => $value) {
            imagejpeg(imagecrop($source_img, $value), $tmpDirFull.$baseFileName.'_0'.$key.'.jpg',100);
          }
          break;
        case '3x3':
          //From left to right, top to bottom
          // 1 - 2 - 3
          // 4 - 5 - 6
          // 7 - 8 - 9
          $con = array();
          $con[1] = array('x' => 0, 'y' => 0, 'width' => $pixels, 'height' => $pixels);
          $con[2] = array('x' => $pixels, 'y' => 0, 'width' => $pixels, 'height' => $pixels);
          $con[3] = array('x' => $pixels * 2, 'y' => 0, 'width' => $pixels, 'height' => $pixels);
          $con[4] = array('x' => 0, 'y' => $pixels, 'width' => $pixels, 'height' => $pixels);
          $con[5] = array('x' => $pixels, 'y' => $pixels, 'width' => $pixels, 'height' => $pixels);
          $con[6] = array('x' => $pixels * 2, 'y' => $pixels, 'width' => $pixels, 'height' => $pixels);
          $con[7] = array('x' => 0, 'y' => $pixels * 2, 'width' => $pixels, 'height' => $pixels);
          $con[8] = array('x' => $pixels, 'y' => $pixels * 2, 'width' => $pixels, 'height' => $pixels);
          $con[9] = array('x' => $pixels * 2, 'y' => $pixels * 2, 'width' => $pixels, 'height' => $pixels);
          foreach ($con as $key => $value) {
            imagejpeg(imagecrop($source_img, $value), $tmpDirFull.$baseFileName.'_0'.$key.'.jpg',100);
          }
          break;
        default:
          echo 'Grid not found, try again';
          exit;
      }
      
      imagedestroy($source_img);
      unlink($srcFile);
      /*
       * Generate Image on the fly
      ob_start();
      imagejpeg($source_img,NULL,100);
      imagedestroy($source_img);
      $source_jpg = ob_get_clean();
      
      echo "<img src='data:image/jpeg;base64,".base64_encode($source_jpg)."'>";
       * 
       */
      
      $zipFile = './tmp/'.$newdir.'.zip';
      $zip = new ZipArchive();
      $zip->open($zipFile, ZipArchive::CREATE);
      $dirToZip = opendir($tmpDir);
      while($file = readdir($dirToZip)) {
        if(is_file($tmpDir.$file) && pathinfo($tmpDir.$file, PATHINFO_EXTENSION) == 'jpg') {
          $zip->addFile($tmpDir.$file,$file);
        }
      }
      $zip->close();
      closedir($dirToZip);
      unset($file);
      
      $dirToDel = opendir($tmpDir);
      while($file = readdir($dirToDel)) {
        if(is_file($tmpDir.$file)) {
          unlink($tmpDir.$file);
        }
      }
      closedir($dirToDel);
      rmdir($tmpDir);
    }
  }
  
  if(!$upload || !$jpg) {
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Slicer - Salkin Project</title>
  </head>
  <body>
    <h1>Slicer <img src="logo.jpg"></h1>
    <p>Upload a JPG and receiv your sliced picture.</p>
    <p>The grid of the sliced picture will be:</p>
    <table>
      <tr>
        <td> 01 </td><td> 02 </td><td> 03 </td>
      </tr>
      <tr>
        <td> 04 </td><td> 05 </td><td> 06 </td>
      </tr>
      <tr>
        <td> 07 </td><td> 08 </td><td> 09 </td>
      </tr>
    </table>
    <p>1 to 6 for 2 Rows and 3 Columns, 1 to 9 for 3 Rows and 3 Columns. The sliced pictures are all equel in width and height, therefore a very small portion of
    the original picture might be lost. Please provide a picture with the correct proportions: 2:3 (landscape) respectivly 1:1.</p>
    <form action="index.php" method="post" enctype="multipart/form-data">
      <fieldset>
        <input type="file" name="picture"><br>
        <input type="radio" name="grid" value="2x3" id="o2" checked>
        <label for="o2">2 Rows and 3 Columns</label><br>
        <input type="radio" name="grid" value="3x3" id="o3">
        <label for="o3">3 Rows and 3 Columns</label><br>
        <input type="submit" name="Upload">
      </fieldset>
    </form>
    <p>After uploading you will receive a ZIP download. The pictures are stored for the time of processing on the server an will be deleted immediately after the download.</p>
    <p>No tracking or other external resources are used.</p>
    <p><small>a Salkin Project - info[at]salk.in</small></p>
  </body>
</html>
<?php
  } else {
    header('Content-Description: File Transfer');
    header("Content-type: application/zip"); 
    header("Content-Disposition: attachment; filename=$newdir.zip");
    header("Content-length: " . filesize($zipFile));
    header("Pragma: no-cache"); 
    header("Expires: 0"); 
    readfile($zipFile);
    unlink($zipFile);
    exit;
  }